import React from 'react';

import EntityLink from 'ftrack-spark-components/lib/entity_link';

function ExampleComponent({ entity }) {
    return (
        <div>
            <EntityLink link={entity && entity.link} size="large" />
        </div>
    );
}

ExampleComponent.propTypes = {
    entity: React.PropTypes.shape({
        link: React.PropTypes.arrayOf(React.PropTypes.shape({
            id: React.PropTypes.string,
            name: React.PropTypes.string,
        })),
    }),
};

export default ExampleComponent;
