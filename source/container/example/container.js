// :copyright: Copyright (c) 2016 ftrack
/* global window */

import React from 'react';
import Button from 'react-toolbox/lib/button';
import ftrackWidget from 'ftrack-web-widget';
import log from 'loglevel';

import { session } from '../../index.js';
import ExampleComponent from '../../component/example';

import style from './style';

class ExampleContainer extends React.Component {
    constructor() {
        super();
        this.state = { entity: null };
        this.onFtrackWidgetUpdate = this.onFtrackWidgetUpdate.bind(this);
        this.onOpenSidebarClicked = this.onOpenSidebarClicked.bind(this);
        this.onRunActionsClicked = this.onRunActionsClicked.bind(this);

        this.onFtrackWidgetUpdate({
            detail: {
                entity: ftrackWidget.getEntity(),
            },
        });
    }

    componentDidMount() {
        window.addEventListener('ftrackWidgetUpdate', this.onFtrackWidgetUpdate);
    }

    componentWillUnmount() {
        window.removeEventListener('ftrackWidgetUpdate', this.onFtrackWidgetUpdate);
    }

    onOpenSidebarClicked() {
        log.info('Opening sidebar');
        ftrackWidget.openSidebar(
            this.state.entity.__entity_type__,
            this.state.entity.id
        );
    }

    onRunActionsClicked() {
        log.info('Running actions');
        ftrackWidget.openActions([
            {
                type: this.state.entity.__entity_type__,
                id: this.state.entity.id,
            },
        ]);
    }

    onFtrackWidgetUpdate(event) {
        const entity = event.detail.entity;
        session.query(
            `select id, name, link from ${entity.type} where id is "${entity.id}"`
        ).then((response) => {
            this.setState({ entity: response.data[0] });
        });
    }

    render() {
        return (
            <div className={style['example-container']}>
                <section className={style['example-component']}>
                    {
                        this.state.entity
                        ? <ExampleComponent entity={this.state.entity} />
                        : <h5>No entity</h5>
                    }
                </section>
                <div className={style.actions}>
                    <Button
                        icon="brightness_auto"
                        raised
                        onClick={this.onRunActionsClicked}
                    />
                    <Button
                        icon="menu"
                        label="Open sidebar"
                        primary
                        raised
                        onClick={this.onOpenSidebarClicked}
                    />
                </div>
            </div>
        );
    }
}

export default ExampleContainer;
