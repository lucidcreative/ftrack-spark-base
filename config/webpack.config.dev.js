// :copyright: Copyright (c) 2016 ftrack

var webpack = require('webpack');
var path = require('path');
var env = require('./env');
var paths = require('./paths');
var baseConfig = require('./webpack.config.base');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var theme = env['process.env.FTRACK_THEME'] === '"dark"' ? 'dark' : 'light';

module.exports = Object.assign(baseConfig, {
    entry: [
        require.resolve('webpack-dev-server/client') + '?/',
        require.resolve('webpack/hot/dev-server'),
        require.resolve('./polyfills'),
        path.join(paths.source, 'index.js')
    ],
    output: {
        path: paths.build,
        pathinfo: true,
        filename: 'static/js/bundle.js',
        publicPath: '/',
    },
    cache: true,
    devtool: 'eval-source-map',
    plugins: [
        new ExtractTextPlugin({ filename: 'static/css/theme-' + theme + '.css' }),
        new HtmlWebpackPlugin({
            inject: false,
            template: paths.html,
        }),
        new CopyWebpackPlugin([
            { from: paths.static, to: 'static/', ignore: [] },
        ]),
        new webpack.DefinePlugin(env),
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),
        new webpack.ProvidePlugin({
            fetch: 'exports-loader?self.fetch!whatwg-fetch',
        }),
        new webpack.HotModuleReplacementPlugin(),
    ],
});
