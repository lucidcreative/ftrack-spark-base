// :copyright: Copyright (c) 2016 ftrack

var autoprefixer = require('autoprefixer');
var paths = require('./paths');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var env = require('./env');
var theme = env['process.env.FTRACK_THEME'] === '"dark"' ? 'dark' : 'light';

module.exports = {
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                enforce: 'pre',
                use: [{
                    loader: 'eslint-loader',
                    options: {
                        ignore: false,
                    },
                }],
                include: paths.source,
            },
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    ignore: [
                        'static/**',
                    ],
                    presets: [
                        require.resolve('babel-preset-es2015'),
                        require.resolve('babel-preset-react'),
                    ],
                    plugins: [
                        require.resolve('babel-plugin-transform-object-rest-spread'),
                    ],
                }
            },
            {
                test: /(\.scss|\.css)$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                sourceMap: true,
                                importLoaders: 1,
                                localIdentName: "[name]--[local]--[hash:base64:8]",
                            },
                        },
                        {
                            loader: 'postcss-loader',
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                data: '@import "' + paths.theme[theme] + '";',
                            },
                        },
                    ],
                }),
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'image/[path][name].[hash].[ext]',
                            context: paths.source,
                            useRelativePath: true,
                            publicPath: '../../', // relative to index
                        },
                    },
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            bypassOnDebug: true,
                        },
                    },
                ],
            },
        ],
    },
    resolve: {
        extensions: ['*', '.js', '.scss', '.css', '.json'],
        modules: [paths.source, 'node_modules'],
    },
};
