// :copyright: Copyright (c) 2016 ftrack

var webpack = require('webpack');
var path = require('path');
var paths = require('./paths');
var env = require('./env');
var baseConfig = require('./webpack.config.base');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var theme = env['process.env.FTRACK_THEME'] === '"dark"' ? 'dark' : 'light';

if (env['process.env.NODE_ENV'] !== '"production"') {
    throw new Error('Production builds must have NODE_ENV=production.');
}

module.exports = Object.assign(baseConfig, {
    entry: [
        require.resolve('./polyfills'),
        path.join(paths.source, 'index.js'),
    ],
    output: {
        path: paths.build,
        filename: 'static/js/[name].[chunkhash:8].js',
        chunkFilename: 'static/js/[name].[chunkhash:8].chunk.js',
        publicPath: '/',
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true
        }),
        new ExtractTextPlugin({ filename: 'static/css/theme-' + theme + '.css' }),
        new HtmlWebpackPlugin({
            inject: false,
            template: paths.html,
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeRedundantAttributes: true,
                useShortDoctype: true,
                removeEmptyAttributes: true,
                removeStyleLinkTypeAttributes: true,
                keepClosingSlash: true,
                minifyJS: true,
                minifyCSS: true,
                minifyURLs: true,
            },
        }),
        new CopyWebpackPlugin([
            { from: paths.static, to: 'static/' },
        ]),
        new webpack.DefinePlugin(env),
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),
        new webpack.ProvidePlugin({
            fetch: 'exports-loader?self.fetch!whatwg-fetch',
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"production"',
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                screw_ie8: true, // React doesn't support IE8
                warnings: false
            },
            mangle: {
                screw_ie8: true
            },
            output: {
                comments: false,
                screw_ie8: true
            }
        }),
        new webpack.optimize.AggressiveMergingPlugin()
    ]
});
