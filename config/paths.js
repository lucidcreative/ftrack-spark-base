// :copyright: Copyright (c) 2016 ftrack
var path = require('path');

var rootPath = path.resolve(__dirname, '..');
var sourcePath = path.resolve(rootPath, 'source');
var buildPath = path.resolve(rootPath, 'build');

module.exports = {
    source: sourcePath,
    root: rootPath,
    theme: {
        light: path.resolve(sourcePath, 'style', 'theme-light.scss'),
        dark: path.resolve(sourcePath, 'style', 'theme-dark.scss'),
    },
    nodeModules: path.resolve(rootPath, 'node_modules'),
    build: buildPath,
    html: path.resolve(sourcePath, 'index.html'),
    static: path.resolve(sourcePath, 'static'),
};
